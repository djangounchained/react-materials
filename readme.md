# Getting started with React development

## Installation and required tools (Windows)

### Step 1: Visual studio code
Visual studio code is a text editor that makes javascript and react development a breeze.
Download: https://code.visualstudio.com/

### Step 2: Node & NPM
Node is a program that allows you to run javascript outside a browser. It comes with NPM (Node Package Manager). Npm can be used to download, install and upgrade open source code other people made. For example react itself is something installed via NPM. Both Node and NPM are required to start with React development.
Download: https://nodejs.org/en/ (Include chocolaty when installing)

### Step 3: Create react app
Next we can use NPM to download create-react-app. Create react app will setup new React projects that are ready to go.
- Open a terminal (There is a terminal window in visual studio code. Use view > Terminal)
- Run the following command in a terminal: `npm install -g create-react-app`

## Create a new project
To create a react project you can run `npm init react-app YOUR_APP_NAME_HERE`
When it's done it will tell you the commands you need to run your application.
More info on creating react applications here:

https://github.com/facebook/create-react-app