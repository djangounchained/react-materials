import './App.css';
import React, {useState} from 'react'

function App() {
  const [name, setName] = useState(null)
  const [age, setAge] = useState(null)
  const [error, setError] = useState (null)
  const [gender, setGender] = useState(null) 

  console.log(`name: ${name}`)
  console.log(`age; ${age}`)
  console.log(`gender; ${gender}`) 

  const onSubmit = (event) => {
    event.preventDefault()
    setError(null)

    if (event.target.yourName.value) {
      setName(event.target.yourName.value)
    } else {
      setError("Naam niet ingevuld")
      return
    }

    if (event.target.yourAge.value) {
      setAge(event.target.yourAge.value)
    } else {
      setError("Age niet ingevuld")
    }
    setGender(event.target.yourGender.value)
   
  }

  let greeting = null
  if (name) {
    if (gender === "male") {
      greeting = `Hi Mr ${name}`
    } else if (gender === "female") {
      greeting = `Hi Ms ${name}`
    } else {
      greeting = `Hi ${name}`
    }
  }

  return (
    <div className="App">
      <header className="App-header">

        <form onSubmit={onSubmit}>
        {error && (
          <p>
            {error}
          </p>
        )}
          <div> 
            <p>What's your name?</p>
            <input type="text" name="yourName" />
            <p> What's your age?</p>
            <input type="number" name="yourAge" />
            <p>Gender?</p>
            <select name="yourGender">
              <option value="">--</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
            </select>
          </div>
          <button type="submit">Submit</button>
        </form>

        {greeting && (
          <p>
            {greeting}<br />
          </p>
        )}
        
        {age && (
          <p>Your age is {age}</p>
        )}
        
        {/* {age < 18 && (
          <p>This site is for 18 years and older only</p>
        )} */}
      </header>
    </div>
  );
}

export default App;
